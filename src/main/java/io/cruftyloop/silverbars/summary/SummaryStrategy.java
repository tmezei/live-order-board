package io.cruftyloop.silverbars.summary;

import io.cruftyloop.silverbars.order.NewOrder;

import java.util.stream.Stream;

public interface SummaryStrategy {

  void generate(Stream<NewOrder> stream);
}
