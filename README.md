# silverbars-live-order-board

## Technical decisions

- orders modeled as final classes, Cancel wraps NewOrder for simplicity
- routing of orders to the order store is handed through a middleware `Bus` implementation
- currently `Bus` has two implementations, just to show usefulness, a direct (map based) and a RxJava-backed
- price per kg is modeled as a long, quantity in kg is modeled as a double (no units/measurement system for simplicity)
- `NewOrder.getRelativePricePerKg()` returns negative values for buys, positives for sells
- `SortedPrintingSummary` relies on this information
- main class is `io.cruftyloop.silverbars.Application`

## Building the application
- the project uses Gradle for assembly
- to build the project, run `gradle build` on the command line, this runs the tests and creates the distribution:
  - `build/libs/live-order-board.jar`
  - `build/distributions/live-order-board.zip` that contains the executable project
  
## Running the application
- running the application can be done in an IDE
- or by running `gradle run` on the command line
- or by unzipping the previously built zip archive and running `bin/live-order-board` (*NIX) or `bin/live-order-board.bat` (Windows)  