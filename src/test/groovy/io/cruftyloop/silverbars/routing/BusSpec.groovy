package io.cruftyloop.silverbars.routing

import spock.lang.Specification

class BusSpec extends Specification {

  def "accepts and publishes requests"() {
    given:
    def accepted = false

    and:
    bus.subscribe(Object.class, { accepted = true })

    when:
    bus.post(Mock(Object))

    then:
    accepted

    where:
    bus << [new RxBus(), new SyncBus()]
  }

  def "routes to the correct subscriber"() {
    given:
    def longAccepted = false
    def stringAccepted = false

    and:
    bus.subscribe(Long.class, { longAccepted = !longAccepted })

    and:
    bus.subscribe(String.class, { stringAccepted = !stringAccepted })

    when:
    bus.post("boo")

    then:
    !longAccepted
    stringAccepted

    when:
    bus.post(2L)

    then:
    longAccepted
    stringAccepted

    where:
    bus << [new RxBus(), new SyncBus()]
  }

}