package io.cruftyloop.silverbars.routing;

import java.util.function.Consumer;

public interface Bus {

  void post(Object event);

  <E> void subscribe(Class<E> eventClass, Consumer<E> onNext);
}
