package io.cruftyloop.silverbars.order

import spock.lang.Specification

import static groovy.util.GroovyCollections.combinations

class NewOrderSpec extends Specification {

  def "user id cannot be null"() {
    when:
    new NewOrder(null, 1.0D, 1L, type)

    then:
    thrown(NullPointerException)

    where:
    type << [OrderType.BUY, OrderType.SELL]
  }

  def "quantity and price should be positive"() {
    when:
    new NewOrder("user1", 1.0D, 1L, type)

    then:
    notThrown(Exception)

    where:
    type << [OrderType.BUY, OrderType.SELL]
  }

  def "zero or negative values for quantity or price are not supported"() {
    when:
    new NewOrder("user1", quantity as double, price as long, type as OrderType)

    then:
    thrown(Exception)

    where:
    [type, quantity, price] << combinations([OrderType.BUY, OrderType.SELL], [0, -1.0D], [1L]) +
        combinations([OrderType.BUY, OrderType.SELL], [0, -1.0D], [0L, -1L]) +
        combinations([OrderType.BUY, OrderType.SELL], [1.0D], [0, -1L]) +
        combinations([OrderType.BUY, OrderType.SELL], [0, -1.0D], [0, -1L])
  }
}