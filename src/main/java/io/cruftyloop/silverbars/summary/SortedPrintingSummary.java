package io.cruftyloop.silverbars.summary;

import io.cruftyloop.silverbars.order.NewOrder;

import java.io.PrintStream;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class SortedPrintingSummary implements SummaryStrategy {

  private final PrintStream out;

  public SortedPrintingSummary(PrintStream out) {
    this.out = out;
  }

  @Override
  public void generate(Stream<NewOrder> stream) {
    SortedMap<Long, Double> summary = stream.collect(Collectors.toMap(NewOrder::getRelativePricePerKg, NewOrder::quantityInKg, (q1, q2) -> q1 + q2, TreeMap::new));
    SortedMap<Long, Double> buys = summary.subMap(Long.MIN_VALUE, 0L);
    SortedMap<Long, Double> sells = summary.subMap(0L, Long.MAX_VALUE);

    out.println("BUY");
    buys.forEach((price, quantity) -> out.println(String.format("%.1f kg for £%d", quantity, Math.abs(price))));
    out.println("SELL");
    sells.forEach((price, quantity) -> out.println(String.format("%.1f kg for £%d", quantity, price)));
    out.println();
    out.flush();
  }
}
