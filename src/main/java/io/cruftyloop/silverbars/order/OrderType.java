package io.cruftyloop.silverbars.order;

public enum OrderType {
  BUY {
    @Override
    public long relativize(long price) {
      return price < 0 ? price : -price;
    }
  },
  SELL {
    @Override
    public long relativize(long price) {
      return price > 0 ? price : -price;
    }
  };

  public abstract long relativize(long price);
}
