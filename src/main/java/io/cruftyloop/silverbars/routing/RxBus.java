package io.cruftyloop.silverbars.routing;

import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

import java.util.function.Consumer;

public class RxBus implements Bus {

  private final Subject<Object> subject = PublishSubject.create().toSerialized();

  @Override
  public void post(Object event) {
    subject.onNext(event);
  }

  @Override
  public <E> void subscribe(Class<E> eventClass, Consumer<E> onNext) {
    subject.filter(event -> eventClass.isAssignableFrom(event.getClass()))
           .cast(eventClass)
           .subscribe(onNext::accept);
  }
}
