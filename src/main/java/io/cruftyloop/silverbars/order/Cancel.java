package io.cruftyloop.silverbars.order;

public final class Cancel {

  private final NewOrder newOrder;

  public Cancel(NewOrder newOrder) {
    this.newOrder = newOrder;
  }

  public NewOrder getNewOrder() {
    return newOrder;
  }
}
