package io.cruftyloop.silverbars.order;

import java.util.Objects;

public final class NewOrder {

  private final String userId;
  private final double quantityInKg;
  private final long pricePerKg;
  private final OrderType type;

  public NewOrder(String userId, double quantityInKg, long pricePerKg, OrderType type) {
    this.userId = Objects.requireNonNull(userId);
    this.quantityInKg = requirePositive(quantityInKg, "quantityInKg <= 0");
    this.pricePerKg = requirePositive(pricePerKg, "getPricePerKg <= 0");
    this.type = Objects.requireNonNull(type);
  }

  public long getPricePerKg() {
    return pricePerKg;
  }

  public long getRelativePricePerKg() {
    return type.relativize(pricePerKg);
  }

  public double quantityInKg() {
    return quantityInKg;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    NewOrder newOrder = (NewOrder) o;

    if (Double.compare(newOrder.quantityInKg, quantityInKg) != 0) return false;
    if (pricePerKg != newOrder.pricePerKg) return false;
    if (!userId.equals(newOrder.userId)) return false;
    return type == newOrder.type;
  }

  @Override
  public int hashCode() {
    int result;
    long temp;
    result = userId.hashCode();
    temp = Double.doubleToLongBits(quantityInKg);
    result = 31 * result + (int) (temp ^ (temp >>> 32));
    result = 31 * result + (int) (pricePerKg ^ (pricePerKg >>> 32));
    result = 31 * result + type.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return "NewOrder{" +
        "userId='" + userId + '\'' +
        ", quantityInKg=" + quantityInKg +
        ", getPricePerKg=" + pricePerKg +
        ", type=" + type +
        '}';
  }

  private long requirePositive(long num, String msg) {
    if (num <= 0) throw new IllegalArgumentException(msg);

    return num;
  }

  private double requirePositive(double num, String msg) {
    if (num <= 0) throw new IllegalArgumentException(msg);

    return num;
  }
}
