package io.cruftyloop.silverbars;

import io.cruftyloop.silverbars.order.Cancel;
import io.cruftyloop.silverbars.order.NewOrder;
import io.cruftyloop.silverbars.order.OrderType;
import io.cruftyloop.silverbars.routing.Bus;
import io.cruftyloop.silverbars.routing.SyncBus;
import io.cruftyloop.silverbars.store.InMemoryOrderStore;
import io.cruftyloop.silverbars.store.ReadWriteOrderStore;
import io.cruftyloop.silverbars.summary.SortedPrintingSummary;

public class Application {

  public static void main(String[] args) {
    Bus bus = new SyncBus();
    /*Bus bus = new RxBus();*/
    ReadWriteOrderStore store = new InMemoryOrderStore();

    bus.subscribe(NewOrder.class, store::register);
    bus.subscribe(Cancel.class, store::cancel);
    bus.subscribe(SortedPrintingSummary.class, store::summarizeTo);

    bus.post(new NewOrder("user1", 3.5, 306, OrderType.SELL));
    bus.post(new NewOrder("user2", 1.2, 310, OrderType.SELL));
    bus.post(new NewOrder("user3", 1.5, 307, OrderType.SELL));
    bus.post(new NewOrder("user4", 2.0, 306, OrderType.SELL));

    bus.post(new NewOrder("user5", 4.0, 280, OrderType.BUY));
    bus.post(new NewOrder("user6", 5.0, 290, OrderType.BUY));

    bus.post(new SortedPrintingSummary(System.out));
  }
}
