package io.cruftyloop.silverbars.store

import io.cruftyloop.silverbars.order.Cancel
import io.cruftyloop.silverbars.order.NewOrder
import io.cruftyloop.silverbars.order.OrderType
import io.cruftyloop.silverbars.summary.SummaryStrategy
import spock.lang.Specification

import java.util.stream.Collectors

class InMemoryOrderBoardSpec extends Specification {

  def "registered orders appear in the summary"() {
    given:
    def store = new InMemoryOrderStore()
    def summary = []
    def order = new NewOrder(userId, quantity, price, type)

    when:
    store.register(order)

    and:
    store.summarizeTo({ summary = it.collect(Collectors.toList()) })

    then:
    summary.size() == 1
    summary[0] == order

    where:
    userId  | quantity | price | type
    "user1" | 1.0D     | 2L    | OrderType.BUY
    "user1" | 1.0D     | 2L    | OrderType.SELL
  }

  def "can cancel orders"() {
    given:
    def store = new InMemoryOrderStore()
    def order = new NewOrder(userId, quantity, price, type)
    def additionalOrder = new NewOrder("user2", 10.0D, price, type)
    def summary = []

    when:
    store.register(order)
    store.register(additionalOrder)

    and:
    store.summarizeTo({ summary = it.collect(Collectors.toList()) })

    then:
    summary.size() == 2
    summary.containsAll([order, additionalOrder])

    when:
    store.cancel(new Cancel(additionalOrder))

    and:
    store.summarizeTo({ summary = it.collect(Collectors.toList()) })

    then:
    summary.size() == 1
    summary[0] == order

    where:
    userId  | quantity | price | type
    "user1" | 1.0D     | 2L    | OrderType.BUY
    "user1" | 1.0D     | 2L    | OrderType.SELL
  }

  def "streams all registered orders to the summary"() {
    given:
    def store = new InMemoryOrderStore()

    and:
    def summary = Mock(SummaryStrategy)

    when:
    (1..n).each {
      store.register(new NewOrder(userId, quantity, price, randomOrderType()))
    }

    and:
    store.summarizeTo(summary)

    then:
    1 * summary.generate({ it.count() == n })

    where:
    userId  | quantity | price | n
    "user1" | 1.0D     | 2L    | 99
  }

  private static randomOrderType() {
    [OrderType.BUY, OrderType.SELL][new Random().nextInt(2)]
  }
}