package io.cruftyloop.silverbars.store;

import io.cruftyloop.silverbars.order.Cancel;
import io.cruftyloop.silverbars.order.NewOrder;

public interface WriteOrderStore {

  void register(NewOrder order);

  void cancel(Cancel cancel);
}
