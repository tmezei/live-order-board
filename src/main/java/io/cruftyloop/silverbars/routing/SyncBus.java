package io.cruftyloop.silverbars.routing;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Consumer;

public class SyncBus implements Bus {

  private final ConcurrentMap<Class, Consumer> subscribers = new ConcurrentHashMap<>();

  @SuppressWarnings("unchecked")
  @Override
  public void post(Object event) {
    subscribers.keySet()
               .stream()
               .filter(c -> c.isAssignableFrom(event.getClass()))
               .map(subscribers::get)
               .findFirst()
               .ifPresent(consumer -> consumer.accept(event));
  }

  @Override
  public <E> void subscribe(Class<E> eventClass, Consumer<E> onNext) {
    subscribers.put(eventClass, onNext);
  }
}
