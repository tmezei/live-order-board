package io.cruftyloop.silverbars.store;

public interface ReadWriteOrderStore extends WriteOrderStore, ReadOrderStore {
}
