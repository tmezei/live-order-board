package io.cruftyloop.silverbars.summary

import io.cruftyloop.silverbars.order.NewOrder
import io.cruftyloop.silverbars.order.OrderType
import spock.lang.Specification

class SortedPrintingSummarySpec extends Specification {

  def "prints buys in descending, sells in ascending order"() {
    given:
    def baos = new ByteArrayOutputStream()
    def out = new PrintStream(baos)
    def summary = new SortedPrintingSummary(out)

    when:
    summary.generate(orders.stream())

    then:
    baos.toString("UTF-8") == '''\
BUY
2.0 kg for £2
4.2 kg for £1
SELL
3.5 kg for £1
3.0 kg for £2

'''

    where:
    orders = [new NewOrder("user1", 1.0D, 1L, OrderType.BUY), new NewOrder("user1", 1.0D, 2L, OrderType.SELL),
              new NewOrder("user1", 3.2D, 1L, OrderType.BUY), new NewOrder("user1", 2.0D, 2L, OrderType.SELL),
              new NewOrder("user1", 3.5D, 1L, OrderType.SELL), new NewOrder("user1", 2.0D, 2L, OrderType.BUY)]
  }

}