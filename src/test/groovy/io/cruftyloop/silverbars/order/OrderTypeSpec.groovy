package io.cruftyloop.silverbars.order

import spock.lang.Specification

class OrderTypeSpec extends Specification {

  def "when relativized, buys are pushed to the left side of zero, sells stay on the right"() {
    expect:
    OrderType.BUY.relativize(anyPositiveNumber) == -anyPositiveNumber

    and:
    OrderType.SELL.relativize(anyPositiveNumber) == anyPositiveNumber

    where:
    anyPositiveNumber << (1..1000).each { Math.abs(new Random().nextLong()) }
  }
}