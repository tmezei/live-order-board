package io.cruftyloop.silverbars.store;

import io.cruftyloop.silverbars.summary.SummaryStrategy;
import io.cruftyloop.silverbars.order.Cancel;
import io.cruftyloop.silverbars.order.NewOrder;

import java.util.Comparator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;

public class InMemoryOrderStore implements ReadWriteOrderStore {

  private final BlockingQueue<NewOrder> newOrders = new PriorityBlockingQueue<>(11, Comparator.comparingLong(NewOrder::getRelativePricePerKg));

  @Override
  public void register(NewOrder order) {
    newOrders.offer(order);
  }

  @Override
  public void cancel(Cancel order) {
    newOrders.remove(order.getNewOrder());
  }

  @Override
  public void summarizeTo(SummaryStrategy summary) {
    summary.generate(newOrders.stream());
  }
}
