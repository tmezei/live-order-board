package io.cruftyloop.silverbars.store;

import io.cruftyloop.silverbars.summary.SummaryStrategy;

public interface ReadOrderStore {

  void summarizeTo(SummaryStrategy summary);
}
